import os
import argparse
import utils
import pypeliner
import pypeliner.managed as mgd
from workflows import strelka


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    pypeliner.app.add_arguments(parser)

    parser.add_argument('--bams_file',
                        required=True,
                        help='''Path to input bam CSV.''')
    parser.add_argument('--config_file',
                        required=True,
                        help='''Path to yaml config file.''')
    parser.add_argument('--out_dir',
                        required=True,
                        help='''Path to output files.''')

    args = vars(parser.parse_args())

    return args


def main():
    args = parse_args()
    config = utils.load_config(args)

    pyp = pypeliner.app.Pypeline(config=args)

    tumour_bam, normal_bam, sample_ids = utils.read_bams_file(args['bams_file'])

    workflow = pypeliner.workflow.Workflow()

    workflow.setobj(
        obj=mgd.OutputChunks('sample_id'),
        value=tumour_bam.keys(),
    )

    tumour_bai = dict([(sample_id, str(tumour_bam[sample_id]) + '.bai')
                         for sample_id in sample_ids])
    normal_bai = dict([(sample_id, str(normal_bam[sample_id]) + '.bai')
                         for sample_id in sample_ids])

    output_dir = os.path.join(args['out_dir'], '{sample_id}')

    strelka_snv_vcf = os.path.join(output_dir, 'strelka_snv.vcf')
    strelka_indel_vcf = os.path.join(output_dir, 'strelka_indel.vcf')
    strelka_snv_csv = os.path.join(output_dir, 'strelka_snv.csv')
    strelka_indel_csv = os.path.join(output_dir, 'strelka_indel.csv')

    workflow.subworkflow(
        name='strelka',
        func=strelka.create_strelka_workflow,
        axes=('sample_id',),
        args=(
            mgd.InputFile('normal_bam', 'sample_id', fnames=normal_bam),
            mgd.InputFile('normal_bai', 'sample_id', fnames=normal_bai),
            mgd.InputFile('tumour_bam', 'sample_id', fnames=tumour_bam),
            mgd.InputFile('tumour_bai', 'sample_id', fnames=tumour_bai),
            config['reference_genome'],
            mgd.OutputFile(strelka_indel_vcf, 'sample_id'),
            mgd.OutputFile(strelka_snv_vcf, 'sample_id'),
            mgd.OutputFile(strelka_indel_csv, 'sample_id'),
            mgd.OutputFile(strelka_snv_csv, 'sample_id'),
            config,        
        ),
    )

    pyp.run(workflow)

if __name__ == '__main__':
    main()
