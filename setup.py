from setuptools import setup, find_packages
import versioneer
import os







def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths

# extra_files = package_files('mutationseq/workflows/mutationseq/scripts/museqportrait_v0.99.9/')
# extra_files += package_files('mutationseq/workflows/mutationseq/scripts/tabix-0.2.6/')
# extra_files = extra_files +  ['scripts/*.py', 'scripts/*.R', ]
extra_files = ['scripts/*.py', 'scripts/*.R', ]

setup(
    name='strelka',
    packages=find_packages(),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='Strelka pipeline',
    author='',
    author_email='',
    entry_points={'console_scripts': ['strelka = strelka.run:main']},
    package_data={'': extra_files},
)
